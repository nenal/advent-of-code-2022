// aoc2022_01 project main.go
package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func max(ar []int) (m int) {
	m = math.MinInt
	for _, item := range ar {
		if item > m {
			m = item
		}
	}
	return
}

func read_lines() (elfs []int) {
	f, err := os.Open("./01.dat")
	check(err)
	scanner := bufio.NewScanner(f) // f is the *os.File
	elfs = []int{}
	elf := 0
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			elfs = append(elfs, elf)
			//fmt.Println(elf)
			elf = 0
			continue
		}
		val, err := strconv.Atoi(line)
		check(err)
		elf += val
	}
	f.Close()
	return
}

func main() {
	elfs := read_lines()
	fmt.Println(max(elfs))
}
